import React, {Component} from 'react';
import {connect} from "react-redux";

import './AddPost.css';
import Button from "../UI/Button/Button";

import {savePost} from "../../store/actions";

class AddPost extends Component {
  state = {
    title: '',
    description: '',
    image: ''
  };

  inputChangeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({[name]: value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  confirmFormHandler = event => {
    event.preventDefault();

    if (this.state.title === '' || this.state.description === '') return null;

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmitForm(formData);

    this.setState({title: '', description: '', image: ''});
    this.props.history.goBack();
  };

  render() {
    return (
      <div className="AddPost">
        <div className="PageTitle">
          <h2 className="Title">Add new post</h2>
        </div>
        <form action="" className="AddPostForm">
          <div className="FormRow">
            <label>Title
              <input type="text" name="title" onChange={(e) => this.inputChangeHandler(e)}/>
            </label>
          </div>
          <div className="FormRow">
            <label>Content
              <textarea name="description" onChange={(e) => this.inputChangeHandler(e)}/>
            </label>
          </div>
          <div className="FormRow">
            <label>Image
              <input type="file" name="image" onChange={(e) => this.fileChangeHandler(e)}/>
            </label>
          </div>
          <div className="FormRow">
            <Button clicked={(e) => this.confirmFormHandler(e)}>SAVE</Button>
          </div>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmitForm: post => dispatch(savePost(post))
  };
};

export default connect(null, mapDispatchToProps)(AddPost);