import React from 'react';
import './PostSample.css';

const PostSample = props => {
  return (
    <div className="PostSample">
      {props.postData.image ?
        <div className="ImageWrap">
          <img src={`http://localhost:8000/uploads/${props.postData.image}`} alt="" />
        </div>
      : null}
      <div className="PostContent">
        <h4 className="PostTitle">{props.postData.title}</h4>
        <span className="PostDate">{props.postData.datetime}</span>
        <span className="PostReadMore" onClick={props.readMore}>Read Full Post >></span>
      </div>
      <div><span className="RemovePost" onClick={props.removePost}>Delete</span></div>
    </div>
  );
};

export default PostSample;