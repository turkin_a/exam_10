import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import './SinglePost.css';
import Button from "../UI/Button/Button";

import {addComment, getSingle, removeComment} from "../../store/actions";
import CommentBox from "../CommentBox/CommentBox";

class SinglePost extends Component {
  state = {
    newsId: '',
    author: '',
    description: ''
  };

  inputChangeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({[name]: value});
  };

  confirmFormHandler = event => {
    event.preventDefault();
    const comment = {...this.state};
    this.setState({author: '', description: ''});
    this.props.onAddComment(comment);
  };

  componentDidMount() {
    this.props.onGetSinglePost(this.props.match.params.id);
    this.setState({newsId: this.props.match.params.id});
  }

  render() {
    return (
      <Fragment>
        <div className="SinglePost">
          {this.props.selectedPost.image ?
            <div className="ImageWrap">
              <img src={`http://localhost:8000/uploads/${this.props.selectedPost.image}`} alt="" />
            </div>
            : null}
          <h2 className="PostTitle">{this.props.selectedPost.title}</h2>
          <div className="PostDate">{this.props.selectedPost.datetime}</div>
          <div className="PostContent">{this.props.selectedPost.description}</div>
        </div>
        <div className="Comments">
          <h2 className="PostTitle">Comments</h2>
          {this.props.selectedPost.comments ? this.props.selectedPost.comments.map((comment, index) => (
            <CommentBox key={index} data={comment}
                        deleteComment={() => this.props.onRemoveComment(this.state.newsId, comment.id)} />
          )) : null}
        </div>
        <div className="AddComment">
          <h2 className="PostTitle">Add Comment</h2>
        </div>
        <form action="" className="AddCommentForm">
          <div className="FormRow">
            <label>Name
              <input type="text" name="author" onChange={(e) => this.inputChangeHandler(e)}/>
            </label>
          </div>
          <div className="FormRow">
            <label>Comment
              <textarea name="description" onChange={(e) => this.inputChangeHandler(e)}/>
            </label>
          </div>
          <div className="FormRow">
            <Button clicked={(e) => this.confirmFormHandler(e)}>ADD</Button>
          </div>
        </form>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    selectedPost: state.selectedPost
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetSinglePost: id => dispatch(getSingle(id)),
    onAddComment: comment => dispatch(addComment(comment)),
    onRemoveComment: (postId, commentId) => dispatch(removeComment(postId, commentId))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SinglePost);