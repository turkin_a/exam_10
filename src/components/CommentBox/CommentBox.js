import React from 'react';
import './CommentBox.css';

const CommentBox = props => {
  return (
    <div className="CommentBox">
      <div className="CommentAuthor">{props.data.author}</div>
      <div className="CommentBody">{props.data.description}</div>
      <span className="CommentRemove" onClick={props.deleteComment}>Delete</span>
    </div>
  )
};

export default CommentBox;