import React, {Component} from 'react';
import {connect} from "react-redux";

import './NewsList.css';
import Button from "../../components/UI/Button/Button";
import PostSample from "../../components/PostSample/PostSample";

import {getNews, removePost} from "../../store/actions";

class NewsList extends Component {
  addPostHandler = event => {
    event.preventDefault();
    this.props.history.push('/add-post');
  };

  readFullPost = id => {
    this.props.history.push(`/news/${id}`);
  };

  componentDidMount() {
    this.props.onGetNewsList();
  }

  render() {
    return (
      <div className="NewsList">
        <div className="PageTitle">
          <h2 className="Title">Posts</h2>
          <Button clicked={(e) => this.addPostHandler(e)}>Add new post</Button>
        </div>
        {this.props.newsList.map(post => (
          <PostSample key={post.id} postData={post}
                      readMore={() => this.readFullPost(post.id)}
                      removePost={() => this.props.onRemovePost(post.id)}
          />
        ))}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    newsList: state.newsList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetNewsList: () => dispatch(getNews()),
    onRemovePost: id => dispatch(removePost(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);