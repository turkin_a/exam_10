import React, { Component, Fragment } from 'react';
import {Switch, Route} from "react-router-dom";

import Header from "./components/Header/Header";
import NewsList from "./containers/NewsList/NewsList";
import AddPost from "./components/AddPost/AddPost";
import SinglePost from "./components/SinglePost/SinglePost";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <Switch>
          <Route path="/" exact component={NewsList} />
          <Route path="/news" exact component={NewsList} />
          <Route path="/news/:id" component={SinglePost} />
          <Route path="/add-post" component={AddPost} />
          <Route render={() => <h1>404 Not found</h1>} />
        </Switch>
      </Fragment>
    );
  }
}

export default App;