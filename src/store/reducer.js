import * as actionTypes from './actionTypes';

const initialState = {
  newsList: [],
  selectedPost: {
    comments: []
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_NEWS_LIST:
      return {...state, newsList: action.newsList, selectedPost: {}};
    case actionTypes.GET_SINGLE_POST:
      return {...state, selectedPost: action.post};
    default:
      return state;
  }
};

export default reducer;