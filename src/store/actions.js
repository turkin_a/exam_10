import * as actionTypes from './actionTypes';
import axios from '../axios-api';

export const getNewsList = newsList => {
  return {type: actionTypes.GET_NEWS_LIST, newsList};
};

export const getSinglePost = post => {
  return {type: actionTypes.GET_SINGLE_POST, post};
};

export const getNews = () => {
  return dispatch => {
    return axios.get('/news')
      .then(response => {
        dispatch(getNewsList(response.data));
      }).catch(error => console.log(error))
  }
};

export const getSingle = id => {
  return dispatch => {
    return axios.get(`/news/${id}`)
      .then(response => {
        dispatch(getComments(response.data));
      }).catch(error => console.log(error))
  }
};

export const getComments = post => {
  return dispatch => {
    return axios.get(`/comments?news_id=${post[0].id}`)
      .then(response => {
        const postData = post[0];
        postData.comments = response.data;
        dispatch(getSinglePost(postData));
      }).catch(error => console.log(error))
  }
};

export const savePost = post => {
  return dispatch => {
    return axios.post('/news', post)
      .then(() => dispatch(getNews()))
      .catch(error => console.log(error))
  }
};

export const removePost = id => {
  return dispatch => {
    return axios.delete(`/news/${id}`)
      .then(() => dispatch(getNews()))
      .catch(error => console.log(error))
  }
};

export const addComment = comment => {
  return dispatch => {
    return axios.post('/comments', comment)
      .then(() => dispatch(getSingle(comment.newsId)))
      .catch(error => console.log(error))
  }
};

export const removeComment = (postId, commentId) => {
  return dispatch => {
    return axios.delete(`/comments?id=${commentId}`)
      .then(() => dispatch(getSingle(postId)))
      .catch(error => console.log(error))
  }
};